from django.urls import re_path
from .views import send_request, verify

urlpatterns = [
    re_path(r'^request/$', send_request, name='request'),
    re_path(r'^verify/$', verify , name='verify'),
]