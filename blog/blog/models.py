from django.db import models
from django.utils.html import format_html
from django.utils import timezone
from account.models import User
from django.urls import reverse
from extentions.utils import jalali_converot, persian_numbers_convertor
from django.contrib.contenttypes.fields import GenericRelation
from comment.models import Comment
from star_ratings.models import Rating


class ArticleManager(models.Manager):
    def published(self):
        return self.filter(status='p')


class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status=True)


# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name='عنوان دسته بندی')
    slug = models.SlugField(max_length=100, unique=True, verbose_name='آدرس دسته بندی')
    status = models.BooleanField(default=True, verbose_name='آیا نمایش داده شود؟')
    position = models.IntegerField(verbose_name='پوزیشن')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='children', default=None, blank=True,
                               null=True, verbose_name='زیر دسته')

    class Meta:
        verbose_name = 'دسته بندی'
        verbose_name_plural = 'دسته بندی ها'
        ordering = ['parent__id', 'position']

    def __str__(self):
        return self.title

    objects = CategoryManager()


class IPAddress(models.Model):
    ip_address = models.GenericIPAddressField(verbose_name='آدرس IP')


class Article(models.Model):
    STATUS_CHOICES = (
        ('d', 'پیش نویس'),
        ('p', 'منتشر شده'),
        ('i', 'در حال بررسی'),
        ('b', 'برگشت داده شده'),
    )
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='articles',
                               verbose_name='نویسنده')
    title = models.CharField(max_length=200, verbose_name='عنوان')
    slug = models.SlugField(max_length=100, unique=True, verbose_name='آدرس صفحه')
    category = models.ManyToManyField(Category, verbose_name='دسته بندی', related_name='articles')
    description = models.TextField(verbose_name='محتوا')
    thumbnail = models.ImageField(upload_to='images', verbose_name='عکس اصلی')
    publish = models.DateTimeField(default=timezone.now, verbose_name='زمان انتشار')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name='وضعیت')
    is_special = models.BooleanField(default=False, verbose_name='مقاله ویژه')
    comments = GenericRelation(Comment)
    hits = models.ManyToManyField(IPAddress, through='ArticleHit', blank=True, related_name='hits',
                                  verbose_name='بازدید ها')
    ratings = GenericRelation(Rating)

    def __str__(self):
        return self.title

    def jpublish(self):
        return jalali_converot(self.publish)

    jpublish.short_description = 'زمان انتشار'

    def category_published(self):
        return self.category.filter(status=True)

    def thumbnail_tag(self):
        return format_html(
            '<img style="max-width:100px;border-radius: 5px;" height="75px" src="%s"/>' % self.thumbnail.url)

    thumbnail_tag.short_description = 'عکس'

    class Meta:
        verbose_name = 'مقاله'
        verbose_name_plural = 'مقالات'
        ordering = ['-publish']

    def get_absolute_url(self):
        return reverse('account:home')

    def category_to_str(self):
        return ', '.join([category.title for category in self.category.active()])

    category_to_str.short_description = 'دسته بندی'

    def get_hits(self):
        return persian_numbers_convertor(str(self.hits.count()))

    objects = ArticleManager()


class ArticleHit(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
