from django.contrib import admin
from .models import Article, Category, IPAddress

# Admin header change
admin.site.site_header = 'پنل مدیریت وبلاگ'


# Register your models here.
def make_published(modeladmin, request, queryset):
    rows_updated = queryset.update(status='p')
    if rows_updated == 1:
        message_bit = 'منتشر شد'
    else:
        message_bit = 'منتشر شدند'
    modeladmin.message_user(request, f'{rows_updated} مقاله {message_bit}')
make_published.short_description = "انتشار مقالات انتخاب شده"


def make_draft(modeladmin, request, queryset):
    rows_updated = queryset.update(status='d')
    if rows_updated == 1:
        message_bit = 'پیش نویس شد'
    else:
        message_bit = 'پیش نویس شدند'
    modeladmin.message_user(request, f'{rows_updated} مقاله {message_bit}')
make_draft.short_description = "پیش نویس شدن مقالات انتخاب شده"

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumbnail_tag', 'slug', 'author', 'jpublish', 'is_special', 'status', 'category_to_str')
    list_filter = ('status', 'publish', 'author', 'is_special')
    search_fields = ('title', 'description', 'category')
    prepopulated_fields = {'slug': ('title',)}
    ordering = ('status', '-publish')
    actions = [make_published, make_draft]



admin.site.register(Article, ArticleAdmin)


def make_active(modeladmn, request, queryset):
    rows_updated = queryset.update(status=True)
    if rows_updated == 1:
        message_bit = 'فعال شد.'
    else:
        message_bit = 'فعال شدند.'
    modeladmn.message_user(request, '%d دسته بندی %s' %(rows_updated, message_bit))
make_active.short_description = 'فعال کردن دسته بندی های انتخاب شده'

def make_deActive(modeladmn, request, queryset):
    rows_updated = queryset.update(status=False)
    if rows_updated == 1:
        message_bit = 'غیر فعال شد.'
    else:
        message_bit = 'غیر فعال شدند.'
    modeladmn.message_user(request, '%d دسته بندی %s' %(rows_updated, message_bit))
make_deActive.short_description = 'غیر فعال کردن دسته بندی های انتخاب شده'


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position', 'title', 'slug', 'parent', 'status')
    list_filter = ('status',)
    search_fields = ('title', 'slug', 'parent')
    prepopulated_fields = {'slug': ('title',)}
    actions = [make_active, make_deActive]


admin.site.register(Category, CategoryAdmin)
admin.site.register(IPAddress)