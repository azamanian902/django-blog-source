from django.urls import path
from .views import (
    ArticleList,
    ArticleDetail,
    CategoryList,
    AuthorList,
    ArticlePreview,
    SearchList
)

app_name = 'blog'
urlpatterns = [
    path('', ArticleList.as_view(), name='home'),
    path('page/<int:page>/', ArticleList.as_view(), name='home'),
    path('article/<slug:slug>/', ArticleDetail.as_view(), name='article'),
    path('preview/<int:pk>/', ArticlePreview.as_view(), name='preview'),
    path('category/<slug:slug>/', CategoryList.as_view(), name='category'),
    path('category/<slug:slug>/<int:page>/', CategoryList.as_view(), name='category'),
    path('author/<str:username>/', AuthorList.as_view(), name='author'),
    path('author/<str:username>/page/<int:page>/', AuthorList.as_view(), name='author'),
    path('search/', SearchList.as_view(), name='search'),
    path('search/page/<int:page>/', SearchList.as_view(), name='search'),
]