from django import template
from django.db.models import Count, Q
from ..models import Category, Article
from extentions.utils import persian_numbers_convertor
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType

register = template.Library()


@register.simple_tag
def title():
    return 'وبلاگ جنگو'


@register.inclusion_tag('blog/partials/category_navbar.html')
def category_navbar():
    return {
        'categories': Category.objects.filter(status=True)
    }


@register.inclusion_tag('blog/partials/sidebar.html')
def popular_articles():
    last_month = datetime.now() - timedelta(days=30)
    return {
        'articles': Article.objects.published().annotate(
            count=Count('hits', filter=Q(articlehit__created__gt=last_month))).order_by('-count', '-publish')[:5],
        'title': 'مقالات پر بازدید ماه'
    }


@register.inclusion_tag('blog/partials/sidebar.html')
def hot_articles():
    last_month = datetime.now() - timedelta(days=30)
    # content_type_id = ContentType.objects.get(app_label='blog', model='article').id
    return {
        'articles': Article.objects.published().annotate(
            count=Count('comments', filter=Q(comments__posted__gt=last_month) and Q(
                comments__content_type_id=7))).order_by('-count', '-publish')[:5],
        'title': 'مقالات داغ ماه',
    }


@register.inclusion_tag('blog/partials/sidebar.html')
def most_points_articles():
    return {
        'articles': Article.objects.filter(ratings__isnull=False).order_by('-ratings__average')[:5],
        'title': 'مقالات پر امتیاز',
    }


@register.inclusion_tag('registration/partials/link.html')
def link(request, link_name, content, classes):
    return {
        'request': request,
        'link_name': link_name,
        'link': f'account:{link_name}',
        'content': content,
        'classes': classes
    }
